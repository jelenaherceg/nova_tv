<div id="header">
	<div class="top-cover">
		<div class="top-image">
			<img src="images/top-image.png">
		</div>
		<div id="last-episode-container" class="mobile-hidden">
			<div>
				<a href="#" class="link"> Pogledaj propušteno na</a>
			</div>
			<div class="icon nova-plus"></div>
		</div>
		<div id="socials" class="">
			<div class="icon social facebook button"></div>
			<div class="icon social whatsapp button mobile-visible"></div>
			<div class="icon social viber button mobile-visible"></div>
		</div>
		<div id="nav">
			<div class="row">
				<div class="col-1-menu ">
					<div id="showtimes">
						<div class="icon nova-tv-logo">
						</div>
						<div id="showtime-top">
							<div class="top-bottom-padding">
								<div class="inline">
									Pon-sri, Pet
								</div>
								<div class="time inline float-right">
									22:30
								</div>
							</div>
						</div>
						<div id="show-title" class="title-font small-screen-hidden">
							<div class="top-bottom-padding">
								<a href="#header">Lud, zbunjen, normalan</a>
							</div>

						</div>
					</div>
				</div>
				<div class="col-2-menu fill-height">
					<div class="align-right fill-height small-screen-align-center">
						<div id="menu">
							<div class="menu-item inline button">
								<a href="#epizode">Epizode</a>
							</div>
							<div class="menu-item inline button">
								<a href="#likovi">Likovi</a>
							</div>
							<div class="menu-item inline button">
								<a href="#oseriji">O seriji</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>