<?php
function CurtainObject($img, $title, $description){
    ?>
    <div class="curtainObject">
        <div class="curtainObject-item curtainObject-absolute-item curtainObject-title">
            <div class="curtainObject-content">
                <?= $title ?>
            </div>
        </div>
        <div class="curtainObject-item curtainObject-absolute-item curtainObject-description button curtain-action">
            <div class="curtainObject-content curtain-content">
                <?= $description ?>
            </div>
        </div>
        <div class="curtainObject-item curtainObject-background">
            <img src="<?= $img ?>">
        </div>
    </div>
    <?php
}
?>

<div class="background">
    <div id="epizode" class="content-item content-padding">
        <div class="content-title">
            Epizode
        </div>

        <div id="episode-container" class="be-slick-sm">
            <?php
            CurtainObject(
                "images/episode-1.png",
                "Lud, zbunjen, normalan. <span class='highlighted'> 17. listopada </span>",
                "S obzirom na to da Izet Vodi Farukove financije, Marija je uvjerena da ga on potkrada."
            );
            ?>
            <?php
            CurtainObject(
                "images/episode-2.png",
                "Lud, zbunjen, normalan, 18. listopada",
                "S obzirom na to da Izet Vodi Farukove financije, Marija je uvjerena da ga on potkrada."
            );
            ?>
            <?php
            CurtainObject(
                "images/1.jpg",
                "Lud, zbunjen, normalan, 19. listopada",
                "Damir je ljubomoran na Barbaru i Mariofila."
            );
            ?>
            <?php
            CurtainObject(
                "images/2.jpg",
                "Lud, zbunjen, normalan, 21. listopada",
                "Fazlinovići su u melankoličnom raspoloženju."
            );
            ?>
            <?php
            CurtainObject(
                "images/3.jpg",
                "Lud, zbunjen, normalan, 24. listopada",
                "U Farukov život ponovo ulazi Senada, ali ovaj put s bebom."
            );
            ?>
        </div>
    </div>
    <div id="likovi" class="content-item content-padding">
        <div class="content-title">
            Likovi
        </div>

        <div id="character-container" class="be-slick">
            <?php
            CurtainObject(
                "images/Izet-img.png",
                "Izet Fazlinović",
                "Stariji gospodin sedamdesetih godina, inženjer u penziji. Još uvijek je veoma vitalan i sklon dobroj čašici."
            );
            ?>
            <?php
            CurtainObject(
                "images/Faruk-img.png",
                "Faruk Fazlinović",
                "Izetov sin. Ima sina Damira s Nevenom koja ga je napustila."
            );
            ?>
            <?php
            CurtainObject(
                "images/Damir-img.png",
                "Damir Fazlinović",
                "Farukov sin, Izetov unuk. Liječnik je a tijekom serije našao se u nekoliko ljubavnih veza."
            );
            ?>

            <?php
            CurtainObject(
                "images/avdija.png",
                "Avdija",
                "Avdija u Farukovoj seriji glumi njemačkog oficira iz Drugog svjetskog rata, Stumbefirera Šilinga."
            );
            ?>

            <?php
            CurtainObject(
                "images/bahro-burek.png",
                "Bahro Burek",
                "Surađuje s Farukom i zajedno rade scenarij za TV seriju."
            );
            ?>

            <?php
            CurtainObject(
                "images/barbara.png",
                "Barbara",
                "Žena s kojom Damir ima sina Džebru."
            );
            ?>

            <?php
            CurtainObject(
                "images/kufe.png",
                "Kufe",
                "Čombetov brat koji se vraća iz Amerike."
            );
            ?>

        </div>
    </div>

    <div id="oseriji" class="content-item content-padding">
        <div class="content-title">
            O seriji
        </div>

        <div class="about-background">
            <div id="about-container" class="justify">
                <div class="bold">
                    Izet, Faruk i Damir, tri su generacije već svima poznate obitelji Fazlinović koju, kao neko prokletstvo, prate problemi sa ženama.
                </div>
                <br>
                U stanu Fazlinovića dogodile su se određene promjene. Marija, kao nova vlasnica stana preuzima potpunu kontrolu kako u stanu, tako i u Farukovom životu. Ona čak odlučuje i o poslovima produkcije Akord. Međutim, njihova idila kratko traje. U stan se, potpuno oporavljen nakon operacije u Švedskoj, vraća Izet.
                <br>
                <br>
                Damir i Barbara se razvode i bore za skrbništvo nad Džebrom. Damir teško podnosi razvod te se ne prestaje opijati. Kako ga je Barbara izbacila iz stana, on se vraća kod Faruka. Odvojenost od Džebre najteže pada Izetu, te se na sve moguće načine pokušava izboriti za vrijeme s unukom.
                <br>
                <br>
                Produkcija Akord započinje snimanje nove serije „Don Žuan sa Alipašinog polja“ po scenariju koji je Izet napisao za vrijeme svog boravka u Švedskoj. Iako je Faruk redatelj, Marija je podijelila uloge, te glave uloge pripadaju Izetu, Barbari i Fufetu. Fufe je Čombetov i Kufetov brat koji se vratio iz Amerike.
            </div>
        </div>

    </div>
</div>