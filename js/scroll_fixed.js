$(document).ready(function () {
    var $obj = $('#nav');
    var top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/auto/, 0));

    $(window).resize(function (){
        top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/auto/, 0));
    });
    $(window).scroll(function (event) {
        var y = $(this).scrollTop();

        if (y >= top) {
            $obj.addClass('fixedTop');
        } else {
            $obj.removeClass('fixedTop');
        }
    });
});
