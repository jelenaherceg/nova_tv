

$(document).ready(function(){
   var slick = ".be-slick";
    var small_slick = ".be-slick-sm";
    var config = {
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true,
        prevArrow: '<button class="nav-left slick-nav-arrow"><div></div></button>',
        nextArrow: '<button class="nav-right slick-nav-arrow"><div></div></button>',
        mobileFirst: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1100,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 728,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 620,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 430,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 0,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
        // centerPadding: '100px',
        // asNavFor: '.slider-for'
    };
    var config_sm ={
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: true,
        prevArrow: '<button class="nav-left slick-nav-arrow"><div></div></button>',
        nextArrow: '<button class="nav-right slick-nav-arrow"><div></div></button>',
        mobileFirst: true,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 728,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 620,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 430,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 0,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
        // centerPadding: '100px',
        // asNavFor: '.slider-for'
    };
    var headerSlideshow = {
        // dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        arrows: false
    };
    $(slick).slick(config);
    $(small_slick).slick(config_sm);
});