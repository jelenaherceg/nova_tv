
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/grid.css">
<link rel="stylesheet" type="text/css" href="css/slick.css">
<link rel="stylesheet" type="text/css" href="css/slick-theme.css">

<link rel="stylesheet" type="text/css" href="build/css/style.css">
<link rel="stylesheet" media="only screen and (max-width: 660px)" href="build/css/small-screen/style.css">
<link rel="stylesheet" media="only screen and (max-width: 900px)" href="build/css/medium-screen/style.css">
<link rel="stylesheet" media="only screen and (min-width: 1270px)" href="build/css/big-screen/style.css">
<link rel="stylesheet" media="only screen and (max-width: 500px)" href="build/css/mobile/style.css">